global productos
productos = list()

class Inventario:
      codigo = ""
      tipo = ""
      marca = ""
      modelo = ""
      cantidad = 0
      precio = 0

def registrarProductos():
    print("Registrd del producto")
    reg = Inventario()
    reg.codigo = input("Ingrese el codigo del producto: ")
    reg.tipo = input("Ingrese el tipo del producto:")
    reg.marca = input("Ingrese la marca del producto: ")
    reg.modelo = input("Ingrese el modelo del producto: ")
    reg.cantidad = int(input("Ingrese la cantidad: "))
    reg.precio = float(input("Ingrese el precio: "))

    productos.append(reg)

def verProductos():
  print("Ver todos los productos")

  for reg in productos:
    print("Codigo: ", reg.codigo, "Tipo: ", reg.tipo, "Marca: ", reg.marca, "Modelo: ", reg.modelo, "Cantidad: ", reg.cantidad, "Precio: ", reg.precio)

def buscarProducto():
  print("Buscar producto")

  filtro = input("Ingrese el codigo a buscar:")

  for reg in productos:
    if reg.codigo == filtro or reg.marca == filtro:
      print(reg.codigo, "-", reg.tipo, "-", reg.marca, "-", reg.modelo, "-", reg.cantidad, "-", reg.precio)

def salir():
  print("Gracias por visitarnos")

def menu():
  op = 0

  while op != 4:
    print("------------------------------")
    print("    SISTEMA DE INVENTARIO     ")
    print("------------------------------")
    print("Menu")
    print("1.- Registrar producto")
    print("2.- Ver productos ingresados")
    print("3.- Buscar producto")
    print("4.- Salir")
    op = input("Digite opcion: ")

    if (op == "1"):
      registrarProductos()
    elif (op == "2"):
      verProductos()
    elif (op == "3"):
      buscarProducto()
    elif (op == "4"):
      salir()

menu()

