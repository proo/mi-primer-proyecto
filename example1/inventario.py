class Producto():
    descripcion = " "
    cantidad = " "
    precio = " "

    def __init__(self, des, can, pre):
        self.descripcion = des
        self.cantidad = cas
        self.precio = pre

# -*- coding: utf -8 -*-
Inventario = 1


def vender_Producto():
    global Inventario
    cantidad = int(input("Cantidad de venta : "))
    if cantidad > Inventario:
        print("No tienes productos suficientes")
    else:
        Inventario -= cantidad


def Reabastecer():
    global Inventario
    cantidad = int(input("Cantidad: "))
    Inventario += cantidad


def ver_Inventario():
    print("Cantidad en existencia ", Inventario)
    print()


while True:
    try:
        print("""
        Menú
        [1] Vender producto
        [2] Reabastecer
        [3] Ver el inventario
        [4] Salir
        """)
        opcion = int(input("¿Qué deseas hacer?: "))
    except ValueError:
        print("Favor de ingresar una opción válida")
    else:
        if opcion < 1 or opcion > 4:
            print("{} no es una opción válida".format(opcion))
            continue
        if opcion == 1:
            vender_Producto()
        elif opcion == 2:
            Reabastecer()
        elif opcion == 3:
            ver_Inventario()
        else:
            break
print("Gracias por su compra")












