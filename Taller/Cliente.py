import mysql.connector
conexion1 = mysql.connector.connect(host="localhost", user="root", passwd="root", database="taller")
class Cliente:
    id = 0
    nombre = ''
    apellido = ''
    estado = 'A'
    dir = ''
    tipo = 'C'
    fecha = '2019'
    increment = 1

    def ingresar(self):
        self.id = self.increment
        if (self.id == 1):
            self.increment = self.increment + 1
        self.nombre = input('Introduzca su nombre: ')
        self.apellido = input('Introduzca su apellido: ')
        self.estado = self.estado
        self.dir = input('Introduzca su localizacion:')
        self.tipo = self.tipo
        self.fecha = self.fecha
        cursor1 = conexion1.cursor()
        sql = "insert into sistema(id_cliente, nombre, apellido, estado, localizacion, tipo_cliente, fecha) values (%s,%s,%s,%s,%s,%s,%s)"
        datos = (self.id, self.nombre, self.apellido, self.estado, self.dir, self.tipo, self.fecha)
        cursor1.execute(sql, datos)
        conexion1.commit()
        print(cursor1.rowcount, "registro insertado")
        conexion1.close()

    def consultar(self):
        self.nombre = input('Nombre: ')
        cursor1 = conexion1.cursor()
        query = ("select * from sistema where nombre = '%s' and tipo_cliente = 'C'"%self.nombre)
        cursor1.execute(query)
        usuario = cursor1.fetchone()
        print(usuario)
        conexion1.close()

    def actualizar(self):
        cursor1 = conexion1.cursor()
        b1 = input("Nombre: ")
        b2 = input("Nuevo Nombre: ")
        query = ("UPDATE sistema SET nombre ='%s' WHERE nombre = '%s'" % (b2,b1))
        cursor1.execute(query)
        conexion1.commit()
        query1 = ('select * from sistema')
        cursor1.execute(query1)
        usuario = cursor1.fetchone()
        print(usuario)
        conexion1.close()

    def eliminar(self):
        cursor1 = conexion1.cursor()
        borrar = input('Ingrese el nombre de la persona: ')
        query = ("delete from sistema where nombre = '%s'"%borrar)
        cursor1.execute(query)
        conexion1.commit()
        query1 = ('select * from sistema')
        cursor1.execute(query1)
        usuario = cursor1.fetchone()
        print(usuario)
        conexion1.close()

class MenuClientes:
    def menu(self):
        op = 0
        while op != 5:
            print("------------------------------")
            print("           Clientes           ")
            print("------------------------------")
            print("Menu")
            print("1.- Crear")
            print("2.- Consultar")
            print("3.- Modificar")
            print("4.- Eliminar")
            print('5.- Regresar')

            op = input('Digite una opción: ')

            if (op == '1'):
                a = Cliente()
                a.ingresar()
            elif (op == '2'):
                b = Cliente()
                b.consultar()
            elif (op == '3'):
                c = Cliente()
                c.actualizar()
            elif (op == '4'):
                d = Cliente()
                d.eliminar()
            elif (op == '5'):
                print('')


