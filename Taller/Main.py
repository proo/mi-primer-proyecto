from Taller.Cliente import MenuClientes
from Taller.Prospecto import MenuProspectos
from Taller.proveedor import MenuProveedor

if __name__ == '__main__':
    op = 0
    while op != 3:
        print("------------------------------")
        print("        Compañia XYZ          ")
        print("------------------------------")
        print("Menu")
        print("1.- Cliente")
        print("2.- Prospecto")
        print("3.- Proveedores")
        print("4.- Salir")

        op = int (input('Digite una opción: '))

        if (op == 1):
            cliente1 = MenuClientes()
            cliente1.menu()
        elif (op == 2):
            prospecto1 = MenuProspectos()
            prospecto1.menu()
        elif (op == 3):
            proveedor1 = MenuProveedor()
            proveedor1.menu()