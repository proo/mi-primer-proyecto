import mysql.connector as mariadb

conexion = mariadb.connect(user='root', password="root",host='localhost', database='poo')

cursor = conexion.cursor()

# cursor.execute('SELECT * FROM colaboradores')
#
# print(cursor.fetchall())

# cursor.execute("INSERT INTO colaboradores VALUES (3, 'Pedro', 'Martes' ,1 )")
#
# conexion.commit()

#
# nombre = input("Nombre: ")
# apellido = input("Apellido: ")

# cursor.execute("INSERT INTO colaboradores VALUES (4,{}, {}, {})".format(nombre, apellido, 1))
# Forma #1 de insertar
# query = ("INSERT INTO colaboradores"\
#                "(id, nombre, apellido, status)"\
#                "VALUES (4, %s, %s, 1)")
# cursor.execute(query, (nombre, apellido))

# query = ("UPDATE  colaboradores SET nombre='Demo' WHERE id=2")
#
# cursor.execute(query)
# conexion.commit()

# query = ("DELETE FROM colaboradores WHERE id = 2")
# cursor.execute(query)
# conexion.commit()
# print(conexion)


cursor.execute("SELECT * FROM colaboradores")

resultado = cursor.fetchall()
for item in resultado:
  print(item)



conexion.close()

