import  mysql.connector as mariadb
class DML:
  user = None
  password = None
  host = None
  db = None
  _conexion = None
  _cursor = None

  def __init__(self, user, passw, host, db):
    self.user = user
    self.password = passw
    self.host = host
    self.db = db
  def conexion(self):
    config = {
      'user': self.user,
      'password': self.password,
      'host': self.host,
      'database': self.db
    }
    self._conexion = mariadb.connect(**config)
    self._cursor = self._conexion.cursor()
  def getColaboradores(self):
    query = ("SELECT * FROM colaboradores")
    self._cursor.execute(query)
    return self._cursor.fetchall()
  def getData(self):
    self.conexion()
    table = input("Ingrese el nombre de la tabla: ")
    query = ("SELECT * FROM "+table)
    self._cursor.execute(query)
    return self._cursor.fetchall()




